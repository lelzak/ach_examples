#!/usr/bin/env perl
 
use strict;
use warnings;
 
use CGI;
use DBI;

use constant SEARCH_LIMIT => 100;


#!--------------------------------------------------
sub search_email {
	
	my ($address) = @_;

	print "Поиск по адресу $address<br/><br/>\n";

	my $dbh = DBI->connect("dbi:Pg:dbname=gazpromb","alex","",
		    { RaiseError => 1, AutoCommit => 0 }) || die $!;

	my $base_query_string = "from (
					select created, int_id, str from message where int_id in 
					  (select int_id from log where address=?)
					UNION
					  select created, int_id, str from log where address=?) t";
	
	my $count_query = "select count(t.*) $base_query_string";
	my $sth = $dbh->prepare($count_query) || die $dbh->errstr;
	$sth->execute($address, $address) or die $dbh->errstr;
	my $records_counter = $sth->fetchrow_hashref->{count};
	
	my $data_query = "select t.* $base_query_string order by int_id, created";

	if ($records_counter > SEARCH_LIMIT) {
		$data_query .= " limit " . SEARCH_LIMIT;
		print "Первые " . SEARCH_LIMIT . " записей из $records_counter <br/><br/>\n"; 
		}

	$sth = $dbh->prepare($data_query) || die $dbh->errstr;
	$sth->execute($address, $address) or die $dbh->errstr;
	
	while (my $row = $sth->fetchrow_hashref) {
		# Из задачи не слишком очевидно, но наверное скобки <> все же не нужны
		# print "&lt;$row->{created}&gt; &lt;$row->{str}&gt;<br/>\n";
		print "$row->{created} $row->{str}<br/>\n";
	}
}


#!--------------------------------------------------
sub print_form {
		
	print <<EOF;
	<html>
	<body>
	
	<h2>Введите адрес для поиска</h2>
	<form method="POST">
	<input type="text" name="email"/>
	</form>
	 
	</body>
	</form>
EOF
	
}


#!--------------------------------------------------

my $q = CGI->new;

print $q->header('text/html; charset=UTF-8');

my $email = $q->param('email');

if ($q->request_method() eq 'GET' or
	$email eq "" or
	! defined $email ) { 
	print_form();
	exit;
}

print "<html><body>\n";

search_email($email);

print "<br/><br/><a href=\"\">Новый поиск</a></body></html>";
