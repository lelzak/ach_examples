#!/usr/bin/perl

use strict;
use warnings;

use DBI;

my $LOGFILE = './data/out';


my $dbh = DBI->connect("dbi:Pg:dbname=gazpromb","alex","",
		    { RaiseError => 1, AutoCommit => 0 }) || die $!;

my $tomessage_query = "INSERT INTO message (created, id, int_id, str) VALUES (?, ?, ?, ?)";
my $tomessage_sth = $dbh->prepare($tomessage_query) || die $dbh->errstr;;

my $tolog_query = "INSERT INTO log (created, int_id, str, address) VALUES (?, ?, ?, ?)";
my $tolog_sth = $dbh->prepare($tolog_query) || die $dbh->errstr;


extract_and_prepare_data($LOGFILE);

$dbh->commit || die $dbh->errstr;
$dbh->disconnect;

print "Done.\n";

exit;

sub extract_and_prepare_data {
	my ($logfile) = @_;
	
	open(LOG, "< $logfile") || die $!;
	while(<LOG>) {
		# print $_;
		chomp $_;
		my $datetime = substr $_, 0, 20, '';
		# print "-$datetime-\n";
		chop $datetime;
		# print "-$datetime-\n";
		# print "$_\n";
		
		my @log_fields = split(' ', $_);
		my ($int_id, $status, $address) = @log_fields;
		# Это запись о прибытии сообщения (в табл. message)?
		if ($status eq '<=') {
			# дополнительный разбор поля id=xxx и запись в таблицу messages
			my $id_raw_string = $log_fields[$#log_fields];
			my ($tmp, $id_value) = split('=', $id_raw_string);
			if ($tmp ne 'id') {
				# не все строки со статусом <= содержат корректные данные по полю id=xxxx
				# условия задачи для этого случая не определены
				# можно куда-нибудь ругнуться и зафиксировать невалидную строку
				next;
				}
			$tomessage_sth->execute($datetime, $id_value, $int_id, $_) or die $dbh->errstr;
			next;
			}
		# Запись в таблицу log
		if ( !($status eq '=>' or $status eq '->' or $status eq '**' or $status eq '==') ) {
			# это общая информация, флаг и адрес в данных отсутствуют
			# приводим к undef явно т.к. в $address могут попасть некорректные данные от разбора строки
			$address = undef;  
			}
		$tolog_sth->execute($datetime, $int_id, $_, $address) or die $dbh->errstr;
	}
}
